# How To Install

## Dependencies

- git
- php
- composer

```
# Extract from archive
tar -xzvf w-fix.tar.gz && cd w-fix

# Install Dependencies
composer update

# Copy env
cp .env.example .env

# Edit your database configuration

# Create tables
php artisan migrate

# Create admin user
php artisan db:seed

# Set uploads folder permissions
sudo chmod -R 777 public/uploads/

# If you see white blank screen, please check 'storage' folder's permissions.
# It should be 775.
# /var/www or application path must be 'www-data' group.
# W-fix application's document root is <path>/w-fix/public

sudo chmod -R 775 storage/
sudo chown -R www-data:www-data storage/

```
