<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>W-Fix</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/flatly/bootstrap.min.css" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('assets/css/extra.css') }}">
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .chatbox-disable {
            width: 200px;
            height: 40px;
            background-color: #fff;
            position: absolute;
            bottom: 0;
            right: 0;
        }

        .chatbox-enable {
            width: 700px;
            height: 350px;
            background-color: #fff;
            position: absolute;
            bottom: 0;
            right: 0;
            border: 1px solid #000;
        }

        .chatbox {
            position: fixed;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" style="padding: 0;" href="{{ url('/') }}">
                    <img src="{{ asset('assets/img/logo.jpg') }}" style="max-height: 60px" alt="">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Homepage</a></li>
                    <li class="{{ Request::is('services') ? 'active' : '' }}"><a href="{{ url('/services') }}">Services</a></li>
                    <li class="{{ Request::is('employees') ? 'active' : '' }}" ><a href="{{ url('/employees') }}">Employees</a></li>
                    <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{ url('/contact') }}">Contact</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                <li><a href="{{ url('/edit_account') }}"><i class="fa fa-btn fa-gear"></i>Edit Account</a></li>
                                 @if ( Auth::user()->is_admin )
                                    <li><a href="{{ url('/admin') }}"><i class="fa fa-desktop"></i> Control Panel</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    <li style="margin-top: 8px">
                        <form action="{{ url('/search') }}">
                            <input type="text" class="form-control" name="q" placeholder="search term">
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
   
    @if ( Auth::check() )
        <div class="chatbox chatbox-disable" id="chatboxDisable">
            <div style="padding-top: 10px; padding-left: 10px; margin-bottom: -10px; background-color: #2C3E50; color: #fff; height: 50px;"><i class="fa fa-comment"></i> <span id="chatboxHeadOpen">Online Support</span></div>
        </div>

        <div class="chatbox chatbox-enable" id="chatboxEnable" style="display: none">
            <div style="padding-top: 10px; padding-left: 10px; margin-bottom: -10px; background-color: #2C3E50; color: #fff; height: 50px;"><i class="fa fa-comment"></i> <span id="chatboxHeadClose">Online Support</span></div>
            <div style="background-color: #fff; width: 100%; height: 275px; padding: 5px;overflow: scroll;" id="supportMessages" date="">
            </div>
            <input type="text" name="message_box" id="messageBox" style="display: inline; border-radius: 0px; width: 560px; float: left" class="form-control input-sm" placeholder="text here">
            <div>
                <div class="btn-group" role="group" aria-label="Basic example" style="display: inline">
                  <button id="imageUpload" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-camera"></i></button>
                  <button id="videoUpload" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-video-camera"></i></button>
                  <button id="audioUpload" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-microphone"></i></button>
                  <button id="send" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-send"></i></button>
                </div>
            </div>
        </div>
    @endif

    <div class="footer">
    <div class="container">
        <ul class="social">
            <li><a href="#"><i class="fa fa-github"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        </ul>
        <ul class="menu">
            <li><a href="{{ url('/') }}">Homepage</a></li>
            <li><a href="{{ url('/services') }}">Services</a></li>
            <li><a href="{{ url('/employees') }}">Employees</a></li>
            <li><a href="{{ url('/contact') }}">Contact</a></li>
        </ul>
        <img src="http://www.gnu.org/graphics/empowered-by-gnu.svg" style="max-width: 10px" alt="">
        <span><a href="http://www.gnu.org/licenses/gpl-3.0.en.html">GPLv3.0</a></span>
    </div>
</div>

    <!-- Modal -->
  <div class="modal fade" id="imageUploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-picture"></span> Image Upload </h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
            <form id="imageUploadForm">
                {!! csrf_field() !!}
              <table>
                <tr>
                  <td><input id="supportFile" name="supportFile" type="file" /></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" value="submit" class="btn btn-primary btn-xs" /> 
                  </td>
                </tr>

              </table>
            </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="videoUploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-facetime-video"></span> video upload</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
         <form id="videoUploadForm">
                {!! csrf_field() !!}
              <table>
                <tr>
                  <td><input id="supportFile" name="supportFile" type="file" /></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" value="submit" class="btn btn-primary btn-xs" /> 
                  </td>
                </tr>

              </table>
            </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="audioUploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-music"></span> Audio upload form</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form id="audioUploadForm">
                {!! csrf_field() !!}
              <table>
                <tr>
                  <td><input id="supportFile" name="supportFile" type="file" /></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" value="submit" class="btn btn-primary btn-xs" /> 
                  </td>
                </tr>

              </table>
            </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
    </div>
</div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script src="{{ asset('assets/js/extra.js') }}"></script>
    <script>
        $(document).ready(function(){
                
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            function App(){};

            var app = new App();

            App.prototype.appendRate = function(e) {
                $.ajax({
                    url: "{{ url('rate_employees') }}",
                    data: { employee_id: $(this).parent().attr('employee-id'), value: $(this).attr('value') },
                    success: function(response){
                        alert('thanks');
                    },
                    error: function(response){
                        console.log(response.responseText);
                    },
                    type: 'GET'
                });
            };

            $('.rating__button').click(app.appendRate);
            $("#openCommentDialog").click(function(){
                var id = $(this).attr('employee-id');
                $('input[name=employee_id]').val(id);
                $("#myModal").modal();

                $.ajax({
                    url: "/get_comments/" + id,
                    success: function(response){
                        $.each(response.comments, function(index, value){
                            $('#comments').append("<p style='float:left;'>" + value.comment + "</p><div style='clear: both'></div><span style='float: left; color: #444'><i>" + value.created_at + "</i></span><div style='clear: both'></div><hr>");
                        });
                    },
                    error: function(response){
                        console.log(response.responseText);
                    },
                    type: 'GET'
                });

            });

            $('#services').change(function(e){
                $('#askPrice').hide();
                var price = $('option:selected', this).attr('price');
                if ( $(this).val() == "0")
                {
                   $('#askPrice').hide();
                } else {
                    $('#askPrice').show();
                }
                $('#price').html('');
                $('#price').append(price + " TL");
                $('#redirectEmployees').attr("href", "/services/" + $(this).val());
            });

            $('#askPrice').click(function(){
                $("#myModal2").modal();
            });

            $('#confirmCallEmployee').click(function(){
                $("#myModal3").modal();
            });

            $('#cancelEmployee').click(function(){
                $("#myModal4").modal();
            });

            $('#chatboxHeadOpen').click(function(){
               $('#chatboxEnable').show();
               $('#chatboxDisable').hide();
            });

            $('#chatboxHeadClose').click(function(){
               $('#chatboxEnable').hide();
               $('#chatboxDisable').show();
            });

            $('#imageUpload').click(function(){
                $("#imageUploadModal").modal();
            });

            $('#videoUpload').click(function(){
                $("#videoUploadModal").modal();
            });

            $('#audioUpload').click(function(){
                $("#audioUploadModal").modal();
            });

            $('#send').click(function(){
                $.ajax({
                    url: "/send_message",
                    dataType: "json",
                    data: { message: $('#messageBox').val(), _token: CSRF_TOKEN, send_to: 1 },
                    success: function(response){
                        $('#messageBox').val('');
                    },
                    error: function(response){
                        console.log(response.responseText);
                    },
                    type: 'POST'
                });
            });

            $.ajax({
                url: "/get_all_messages",
                dataType: "json",
                data: { _token: CSRF_TOKEN },
                success: function(response){
                    console.log(response.messages);
                    date = response.messages[parseInt(response.messages.length)-1].created_at;
                    $('#supportMessages').attr('date', date);
                    $.each(response.messages, function(index, value){
                        $('#supportMessages').append('<div style="padding-bottom: 2px;">' + value.user.name + ':' + value.message + '</div>');
                    });
                },
                error: function(response){
                    console.log(response.responseText);
                },
                type: 'POST'
            });

            setInterval(function(){
                $.ajax({
                    url: "/get_message",
                    dataType: "json",
                    data: { _token: CSRF_TOKEN },
                    success: function(response){
                        if ( response.message.created_at == $('#supportMessages').attr('date') )
                        {

                        } else {
                            $('#supportMessages').append('<div style="padding-bottom: 2px;">' + response.message.user.name + ':' + response.message.message + '</div>');
                            date = response.message.created_at;
                            $('#supportMessages').attr('date', date);
                        }
                    },
                    error: function(response){
                        console.log(response.responseText);
                    },
                    type: 'POST'
                });
            }, 1000);


             $("form#imageUploadForm").submit(function(evt){     
               evt.preventDefault();

               var formData = new FormData($(this)[0]); 

               $.ajax({
                 url: '/image_upload',
                 type: 'POST',
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 enctype: 'multipart/form-data',
                 processData: false,
                 success: function (response) {
                   console.log(response);
                   $('#imageUploadModal').modal("toggle");
                   $('#messageBox').val('<img src="/uploads/' + response.a + '" width="100" height=100>');
                 }
               });

               return false;
             });

             $("form#videoUploadForm").submit(function(evt){     
               evt.preventDefault();

               var formData = new FormData($(this)[0]); 

               $.ajax({
                 url: '/video_upload',
                 type: 'POST',
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 enctype: 'multipart/form-data',
                 processData: false,
                 success: function (response) {
                   console.log(response);
                   $('#videoUploadModal').modal("toggle");
                   $('#messageBox').val('<a target="_blank" href="/uploads/' + response.a + '">' + response.a + '</a>');
                 }
               });

               return false;
             });

              $("form#audioUploadForm").submit(function(evt){     
               evt.preventDefault();

               var formData = new FormData($(this)[0]); 

               $.ajax({
                 url: '/audio_upload',
                 type: 'POST',
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 enctype: 'multipart/form-data',
                 processData: false,
                 success: function (response) {
                   console.log(response);
                   $('#audioUploadModal').modal("toggle");
                   $('#messageBox').val('<a target="_blank" href="/uploads/' + response.a + '">' + response.a + '</a>');
                 }
               });

               return false;
             });


        });
    </script>
</body>
</html>
