@extends('layouts.app')

@section('content')
<!-- Home image -->
<div class="jumbotron home-image">
	<div class="container">
		<h1>Contact <i class="fa fa-exclamation-circle"></i></h1>
	</div>
</div>
<div class="color-balance"></div>
<!-- #Home image -->
	
	<div class="container">
	<h2>Contact Form</h2>
	<hr>
		<form action="/contact" method="POST" role="form">
			{!! csrf_field() !!}
			@if ( Session::has('info') )
				<div class="alert alert-success">{{ Session::get('info')['message'] }}</div>
			@endif
			<div class="form-group">
				<label for="name"><span class="text text-danger">*</span>Your Name:</label>
				<input type="text" name="name" id="name" class="form-control" placeholder="ex: John Doe">
			</div>
			<div class="form-group">
				<label for="email"><span class="text text-danger">*</span>Email:</label>
				<input type="text" name="email" id="email" class="form-control" placeholder="ex: example@mail.com">
			</div>
			<div class="form-group">
				<label for="phone">Phone Number:</label>
				<input type="text" name="phone" id="phone" class="form-control" placeholder="ex: 555 555 55 55">
			</div>
			<div class="form-group">
				<label for="message"><span class="text text-danger">*</span>Message:</label>
				<textarea name="message" id="message" placeholder="ex: bla bla.." class="form-control"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-primary"><i class="fa fa-send"></i> Send</button>
			</div>
		</form>
	</div>
@endsection