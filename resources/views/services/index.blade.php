@extends('layouts.app')

@section('content')
<!-- Home image -->
<div class="jumbotron home-image">
	<div class="container">
		<h1>Our Services <i class="fa fa-exclamation-circle"></i></h1>
	</div>
</div>
<div class="color-balance"></div>
<!-- #Home image -->
<div class="container">
	<div class="row" style="text-align: center">
		<div class="col-md-12">
			<div class="panel panel-default">
			<div class="panel-heading">Choose the Service</div>
				<div class="panel-body">
					<select name="services" style="width: 300px" id="services">
						<option value="0">Please select</option>
						@foreach( $services as $service )
							<option value="{{ $service->id }}" price="{{ $service->price }}">{{ $service->title }}</option>
						@endforeach
					</select>
					<div class="form-group" style="margin-top: 20px">
						<button style="display: none" class="btn btn-primary btn-sm" id="askPrice">Ask Price Quote</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body" style="padding:40px 50px; text-align: center">
         	The total price of the selected service is: 
         	<h2 id="price"></h2>
        </div>
        <div class="modal-footer" id="comments" style="text-align: center;">
        	<button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
        	<a href="" class="btn btn-primary btn-sm" id="redirectEmployees">Accept</a>
        </div>
      </div>
	</div>
</div>
@endsection