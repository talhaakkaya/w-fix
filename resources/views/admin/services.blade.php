@extends('admin.layout')

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="page-header">
			<h1>Services</h1>
		</div>
		<form action="{{ url('/admin/services') }}" method="POST">
			{!! csrf_field() !!}
			<div class="form-group">
				<label for="">Service Name: </label>
				<input type="text" name="service_name" class="form-control" placeholder="ex: repairing washing machine">
			</div>
			<div class="form-group">
                                <label for="">Service Price: </label>
                                <input type="text" name="service_price" class="form-control" placeholder="ex: 10">
                        </div>
			<div class="form-group">
				<button class="btn btn-primary">Save</button>
			</div>
		</form>
	</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Title</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				@foreach( $services as $service )
					<tr>
						<td>{{ $service->title }}</td>
						<td>{{ $service->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
</div>
@endsection
