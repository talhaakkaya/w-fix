@extends('admin.layout')

@section('content')
	<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Send Invoice</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-md-6">
		<form action="{{ url('/admin/send_invoice') }}" method="POST" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div class="form-group">
				<label for="email">Title: </label>
				<input type="text" class="form-control" id="title" placeholder="ex: Boyner" name="invoice_title">
			</div>

			<div class="form-group">
				<label for="email">User Email: </label>
				<input type="text" class="form-control" id="email" placeholder="ex: example@email.com" name="invoice_email">
			</div>

			<div class="form-group">
				<label for="invoice">Select Invoice: </label>
				<input type="file" name="invoice" id="invoice">
			</div>

			<div class="form-group"><button class="btn btn-primary">Send</button></div>
		</form>
		<div class="page-header">
			<h1>Old Invoices</h1>
		</div>
		
	</div>
	<table class="table table-striped">
			<thead>
				<tr>
					<th>Title</th>
					<th>Email</th>
					<th>File</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				@foreach( $invoices as $invoice )
					<tr>
						<td>{{ $invoice->title }}</td>
						<td>{{ $invoice->email }}</td>
						<td><a href="{{ url('/uploads/' . $invoice->file) }}">{{ $invoice->title }}</a></td>
						<td>{{ $invoice->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
</div>
@endsection