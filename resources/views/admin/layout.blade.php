<?php $users = \App\User::where('is_admin', 0)->get(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>W-Fix Company</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/assets/admin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/assets/admin/dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/admin/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/assets/admin/bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/assets/admin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .chatbox-disable {
            width: 200px;
            height: 40px;
            background-color: #fff;
            position: absolute;
            bottom: 0;
            right: 0;
        }

        .chatbox-enable {
            width: 700px;
            height: 350px;
            background-color: #fff;
            position: absolute;
            bottom: 0;
            right: 0;
            border: 1px solid #000;
        }

        .chatbox {
            position: fixed;
        }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">W-Fix</a>
            </div>
            <!-- /.navbar-header -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url('/admin') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/send_invoice') }}"><i class="fa fa-inbox fa-fw"></i> Send Invoice</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/services') }}"><i class="fa fa-gears fa-fw"></i> Services</a>
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}"><i class="fa fa-lock fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            @yield('content')
            @if( Session::has('info') )
                <div class="alert alert-success">{{ Session::get('info')['message'] }}</div>
            @endif

        </div>
        <!-- /#page-wrapper -->
        <div class="chatbox chatbox-disable" style="display: none" id="chatboxDisable">
        <div style="padding-top: 10px; padding-left: 10px; margin-bottom: -10px; background-color: #2C3E50; color: #fff; height: 50px;"><i class="fa fa-comment"></i> <span id="chatboxHeadOpen">Online Support</span></div>
    </div>

    <div class="chatbox chatbox-enable" id="chatboxEnable">
        <div style="padding-top: 10px; padding-left: 10px; margin-bottom: -10px; background-color: #2C3E50; color: #fff; height: 50px;"><i class="fa fa-comment"></i> <span id="chatboxHeadClose">Online Support</span>
            <select name="" style="color: #000" id="currentUser">
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <div style="background-color: #fff; width: 100%; height: 275px; padding: 5px;overflow: scroll;" id="supportMessages" date="">
        </div>
        <input type="text" name="message_box" id="messageBox" style="display: inline; border-radius: 0px; width: 565px; float: left" class="form-control input-sm" placeholder="text here">
        <div>
            <div class="btn-group" role="group" aria-label="Basic example" style="display: inline">
              <button id="imageUpload" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-camera"></i></button>
              <button id="videoUpload" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-video-camera"></i></button>
              <button id="audioUpload" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-microphone"></i></button>
              <button id="send" type="button" class="btn btn-primary btn-sm btn-secondary"><i class="fa fa-send"></i></button>
            </div>
        </div>
    </div>

    <!-- Modal -->
  <div class="modal fade" id="imageUploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-picture"></span> Image Upload </h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
            <form id="imageUploadForm">
                {!! csrf_field() !!}
              <table>
                <tr>
                  <td><input id="supportFile" name="supportFile" type="file" /></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" value="submit" class="btn btn-primary btn-xs" /> 
                  </td>
                </tr>

              </table>
            </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="videoUploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-facetime-video"></span> video upload</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
         <form id="videoUploadForm">
                {!! csrf_field() !!}
              <table>
                <tr>
                  <td><input id="supportFile" name="supportFile" type="file" /></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" value="submit" class="btn btn-primary btn-xs" /> 
                  </td>
                </tr>

              </table>
            </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="audioUploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-music"></span> Audio upload form</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form id="audioUploadForm">
                {!! csrf_field() !!}
              <table>
                <tr>
                  <td><input id="supportFile" name="supportFile" type="file" /></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" value="submit" class="btn btn-primary btn-xs" /> 
                  </td>
                </tr>

              </table>
            </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
    </div>
</div>

    </div>
    <!-- /#wrapper -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/assets/admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->

    <!-- Custom Theme JavaScript -->
    <script src="/assets/admin/dist/js/sb-admin-2.js"></script>
    
    <script>
        $(document).ready(function(){
                
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $('#chatboxHeadOpen').click(function(){
               $('#chatboxEnable').show();
               $('#chatboxDisable').hide();
            });

            $('#chatboxHeadClose').click(function(){
               $('#chatboxEnable').hide();
               $('#chatboxDisable').show();
            });

            $('#imageUpload').click(function(){
                $("#imageUploadModal").modal();
            });

            $('#videoUpload').click(function(){
                $("#videoUploadModal").modal();
            });

            $('#audioUpload').click(function(){
                $("#audioUploadModal").modal();
            });

            $('#send').click(function(){
                $.ajax({
                    url: "/send_message",
                    dataType: "json",
                    data: { message: $('#messageBox').val(), _token: CSRF_TOKEN, send_to: $('#currentUser').val() },
                    success: function(response){
                        $('#messageBox').val('');
                    },
                    error: function(response){
                        console.log(response.responseText);
                    },
                    type: 'POST'
                });
            });

            $.ajax({
                url: "/get_all_messages",
                dataType: "json",
                data: { _token: CSRF_TOKEN },
                success: function(response){
                    console.log(response.messages);
                    date = response.messages[parseInt(response.messages.length)-1].created_at;
                    $('#supportMessages').attr('date', date);
                    $.each(response.messages, function(index, value){
                        $('#supportMessages').append('<div style="padding-bottom: 2px;">' + value.user.name + ':' + value.message + '</div>');
                    });
                },
                error: function(response){
                    console.log(response.responseText);
                },
                type: 'POST'
            });

            setInterval(function(){
                $.ajax({
                    url: "/get_message",
                    dataType: "json",
                    data: { _token: CSRF_TOKEN },
                    success: function(response){
                        if ( response.message.created_at == $('#supportMessages').attr('date') )
                        {

                        } else {
                            $('#supportMessages').append('<div style="padding-bottom: 2px;">' + response.message.user.name + ':' + response.message.message + '</div>');
                            date = response.message.created_at;
                            $('#supportMessages').attr('date', date);
                        }
                    },
                    error: function(response){
                        console.log(response.responseText);
                    },
                    type: 'POST'
                });
            }, 1000);


             $("form#imageUploadForm").submit(function(evt){     
               evt.preventDefault();

               var formData = new FormData($(this)[0]); 

               $.ajax({
                 url: '/image_upload',
                 type: 'POST',
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 enctype: 'multipart/form-data',
                 processData: false,
                 success: function (response) {
                   console.log(response);
                   $('#imageUploadModal').modal("toggle");
                   $('#messageBox').val('<img src="/uploads/' + response.a + '" width="100" height=100>');
                 }
               });

               return false;
             });

             $("form#videoUploadForm").submit(function(evt){     
               evt.preventDefault();

               var formData = new FormData($(this)[0]); 

               $.ajax({
                 url: '/video_upload',
                 type: 'POST',
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 enctype: 'multipart/form-data',
                 processData: false,
                 success: function (response) {
                   console.log(response);
                   $('#videoUploadModal').modal("toggle");
                   $('#messageBox').val('<a target="_blank" href="/uploads/' + response.a + '">' + response.a + '</a>');
                 }
               });

               return false;
             });

              $("form#audioUploadForm").submit(function(evt){     
               evt.preventDefault();

               var formData = new FormData($(this)[0]); 

               $.ajax({
                 url: '/audio_upload',
                 type: 'POST',
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 enctype: 'multipart/form-data',
                 processData: false,
                 success: function (response) {
                   console.log(response);
                   $('#audioUploadModal').modal("toggle");
                   $('#messageBox').val('<a target="_blank" href="/uploads/' + response.a + '">' + response.a + '</a>');
                 }
               });

               return false;
             });


        });
    </script>

</body>

</html>
