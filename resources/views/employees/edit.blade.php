<?php $services = \App\Service::all() ?>
@extends('layouts.app')

@section('content')
<!-- Home image -->
<div class="jumbotron home-image">
        <div class="container">
                <h1>Edit Profile <i class="fa fa-exclamation-circle"></i></h1>
        </div>
</div>
<div class="color-balance"></div>
<!-- #Home image -->
<div class="container">
	<div class="row">
<div style="padding-top: 50px"></div>
			<form action="{{ url('/update_account') }}"  class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
				<input type="hidden" value="PUT" name="_method">
				{!! csrf_field() !!}
					
				<div class="form-group">
					<label for="" class="col-md-4 control-label">Photo</label>
					<div class="col-md-6">
						<img style="float: left; padding-right: 20px; width: 100px; height: 100px" src="{{ get_photo($user->photo) }}" alt="">
						<input name="photo" type="file">
					</div>
				</div>

				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ $user->name }}">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Age</label>

                    <div class="col-md-6">
						<select name="age" id="">
                            @for($i = 17; $i <= 120; $i++)
                                <option value="{{ $i }}"{{ $user->age == $i ? 'selected' : ''}}>{{ $i }}</option>
                            @endfor
                        </select>

                        @if ($errors->has('age'))
                            <span class="help-block">
                                <strong>{{ $errors->first('age') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Address</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="address" value="{{ $user->address }}">

                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Email</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="email" value="{{ $user->email }}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Phone Number</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="phone_number" value="{{ $user->phone_number }}">

                        @if ($errors->has('phone_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                 <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">service</label>

                    <div class="col-md-6">
                        <select name="service" id="">
                           @foreach($services as $service)
                                <option value="{{ $service->id }}">{{ $service->title }}</option>
                           @endforeach
                        </select>

                        @if ($errors->has('service'))
                            <span class="help-block">
                                <strong>{{ $errors->first('service') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i>Save
                        </button>
                    </div>
                </div>
			</form>
	</div>
</div>
@endsection
