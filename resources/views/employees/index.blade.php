@extends('layouts.app')

@section('content')
<!-- Home image -->
<div class="jumbotron home-image">
	<div class="container">
		<h1>Our Employees <i class="fa fa-exclamation-circle"></i></h1>
	</div>
</div>
<div class="color-balance"></div>
<!-- #Home image -->

<div class="container">
	<div class="row">
		@foreach( $users as $user )
		<div class="panel panel-default">
			<div class="panel-heading">
				{{ $user->name }}
			</div>
			<div class="panel-body">
				<img style="float: left; padding-right: 20px; width: 100px; height: 100px" src="{{ get_photo($user->photo) }}" alt="">
				@if ( isset(\App\Service::find($user->service_id)->id) )
					<p>{{ \App\Service::find($user->service_id)->title }}</p>
				@endif
				<p>{{ $user->email }}</p>
				<svg style="display: none;">
				  <symbol id="star" viewBox="0 0 98 92">
				  <title>star</title>
				  <path stroke='#000' stroke-width='5' d='M49 73.5L22.55 87.406l5.05-29.453-21.398-20.86 29.573-4.296L49 6l13.225 26.797 29.573 4.297-21.4 20.86 5.052 29.452z' fill-rule='evenodd'/>
				</svg>

				<div class="rating {{ $user->rate_count != 0 ? 'has--rating' : '' }}" employee-id="{{ $user->id }}">
					<a href="javascript:;" class="rating__button 
						@if( $user->rate_count != 0 )
							{{ (round($user->rating / $user->rate_count)) == 1 ? 'is--active' : '' }}
						@endif
					 " value="1"><svg class="rating__star"><use xlink:href="#star"></use></svg></a>
					<a href="javascript:;" class="rating__button 
						@if( $user->rate_count != 0 )
							{{ (round($user->rating / $user->rate_count)) == 2 ? 'is--active' : '' }}
						@endif
					" value="2"><svg class="rating__star"><use xlink:href="#star"></use></svg></a>
					<a href="javascript:;" class="rating__button 
						@if( $user->rate_count != 0 )
							{{ (round($user->rating / $user->rate_count)) == 3 ? 'is--active' : '' }}
						@endif
					" value="3"><svg class="rating__star"><use xlink:href="#star"></use></svg></a>
					<a href="javascript:;" class="rating__button 
						@if( $user->rate_count != 0 )
							{{ (round($user->rating / $user->rate_count)) == 4 ? 'is--active' : '' }}
						@endif
					 " value="4"><svg class="rating__star"><use xlink:href="#star"></use></svg></a>
					<a href="javascript:;" class="rating__button 
						@if( $user->rate_count != 0 )
							{{ (round($user->rating / $user->rate_count)) == 5 ? 'is--active' : '' }}
						@endif
					 " value="5"><svg class="rating__star"><use xlink:href="#star"></use></svg></a>
				</div>
				<button class="btn btn-info btn-xs pull-right" employee-id="{{ $user->id }}" id="openCommentDialog"><i class="fa fa-comment"></i> Comments</button>
				@if ( isset($buttonName) )
					<a href="/employees/{{ $user->id }}/confirm" style="margin-right: 5px" class="pull-right btn btn-primary btn-xs" ><i class="fa fa-check"></i> {{ $buttonName }}</a>
				@endif
			</div>
		</div>
		@endforeach
	</div>
</div>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-comment"></span> Write your comment</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
         <form action="{{ url('/create_comment') }}" name="createComment" method="POST">
         	{!! csrf_field() !!}
         	<div class="form-group">
         		<input type="hidden" name="employee_id" value="">
         		<textarea name="comment" id="" class="form-control" cols="10" rows="5" placeholder="write your comment here..."></textarea>
         	</div>
         	<div class="btn-group" role="group" aria-label="Basic example">
			  <button type="button" data-dismiss="modal" class="btn btn-secondary btn-danger btn-xs"><i class="fa fa-remove"></i> Cancel</button>
			  <button class="btn btn-secondary btn-primary btn-xs"><i class="fa fa-send"></i> Send</button>
			</div>
         </form>
        </div>
        <div class="modal-footer" id="comments">
        </div>
      </div>
	</div>
</div>
@endsection