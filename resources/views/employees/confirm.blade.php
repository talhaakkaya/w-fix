@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Confirm Employee</div>
				<div class="panel-body" style="font-size: 20px; line-height: 35px">
					<div><span style="font-style: italic;">Name: </span>{{ Auth::user()->name }}</div>
					<div><span style="font-style: italic;">Service Type: </span>{{ $service->title }}</div>
					<div><span style="font-style: italic;">Date: </span>{{ \Carbon\Carbon::now() }}</div>
					<div><span style="font-style: italic;">Price: </span>{{ $service->price }}</div>
					<div><span style="font-style: italic;">Called Employee: </span>{{ $employee->name }}</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary btn-sm" id="confirmCallEmployee">Confirm and Call Employee</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body" style="padding:40px 50px; text-align: center">
        	 <h4>Your service request has been accepted. </h4>
        	 <a href="{{ url('/employees/' . $employee->id . '/cancel') }}" class="btn btn-danger btn-sm">Ok</a>
        </div>
      </div>
	</div>
</div>
@endsection