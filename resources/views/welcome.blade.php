@extends('layouts.app')

@section('content')
<!-- Home image -->
<div class="jumbotron home-image">
    <div class="container">
        <h1>We Fix The World</h1>
        <p class="shadow-text">W-Fix Multi-Services Company is technically based on three main services that are widely used by every person all around the world.Those three main services are plumbing, repairing and housekeeping.That system will be able to used by every individual who has access to internet. You will demand your need online, then, we will assign you our employees with(or without) required tools according to what will be done.</p>
        
    </div>
</div>
<div class="color-balance"></div>
<!-- #Home image -->
@if( Session::has('info') )
    <div class="alert alert-success">{{ Session::get('info')['message'] }}</div>
@endif
<!-- Container -->
<div class="container center">
    <div class="col-md-4">
        <img src="{{ asset('assets/img/s1.jpg') }}" class="border-img" alt="">
        <h4>24/7 availability</h4>
    </div>
    <div class="col-md-4">
        <img src="{{ asset('assets/img/s2.jpg') }}" class="border-img" alt="">
        <h4>Dedicated team</h4>
    </div>
    <div class="col-md-4">
        <img src="{{ asset('assets/img/s3.jpg') }}" class="border-img" alt="">
        <h4>Guranteed satisfaction</h4>
    </div>
</div>
<!-- #Container -->

<!-- Home info -->
<div class="well well-home">
    <div class="container w800">
        <p>Making easier our customers' lives by helping them to solve, their problems related to plumbing, repairing and housekeeping.</p>
    </div>
</div>
<!-- #Home info -->
@endsection
