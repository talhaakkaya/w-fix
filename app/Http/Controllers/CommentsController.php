<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Session;
use App\Http\Requests;

class CommentsController extends Controller
{
    public function store(Request $request)
    {
    	$data = $request->only(['employee_id', 'comment']);
    	Comment::create([
    		'user_id' => $data['employee_id'],
    		'comment' => $data['comment']
    	]);
    	Session::flash('info', ['status' => true, 'message' => 'Your comment was approved.']);
    	return redirect('/');
    }

    public function show($id)
    {
    	$comments = Comment::where('user_id', $id)->orderBy('id', 'DESC')->get();
    	return response()->json(['comments' => $comments]);
    }
}
