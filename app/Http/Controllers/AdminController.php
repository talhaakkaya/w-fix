<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Invoice;
use App\Service;
use App\Http\Requests;

class AdminController extends Controller
{
    
	public function __construct()
	{
		if ( ! Auth::user()->is_admin )
		{
			die('You cannot view this page.');
		}
	}

	public function home()
	{
		return view('admin.homepage');
	}

	public function sendInvoice()
	{
		$invoices = Invoice::all();
		return view('admin.send_invoice', compact('invoices'));
	}

	public function sendInvoicePost(Request $request)
	{
    	$fileName = md5($request->file('invoice')) . '.' . $request->file('invoice')->getClientOriginalExtension();
    	$request->file('invoice')->move(
    		public_path() . '/uploads/', $fileName
    	);
		Invoice::create([
			'title'	=> $request->input('invoice_title'),
			'file'	=> $fileName,
			'email'	=> $request->input('invoice_email')
		]);
		Session::flash('info', ['status' => true, 'message' => 'Your file sent to customer.']);
		return redirect('/admin/send_invoice');
	}

	public function getServices()
	{
		$services = Service::all();
		return view('admin.services', compact('services'));
	}

	public function postServices(Request $request)
	{
		Service::create([
			'title'	=> $request->input('service_name'),
			'price'	=> $request->input('service_price')
		]);
		Session::flash('info', ['status' => true, 'message' => 'Crated new service.']);
		return redirect('/admin/services');
	}

}
