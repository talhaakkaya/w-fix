<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Service;
use App\Http\Requests;
use Auth;
use Session;

class EmployeesController extends Controller
{
    public function index()
    {
    	$users = User::where('is_admin', 0)->paginate(5);
    	return view('employees.index', compact('users'));	
    }

    public function edit()
    {
    	$user = Auth::user();
    	return view('employees.edit', compact('user'));
    }

    public function update(Request $request)
    {
    	if ( $request->has('photo') )
        {
            $fileName = md5($request->file('photo')) . '.' . $request->file('photo')->getClientOriginalExtension();
            $request->file('photo')->move(
                public_path() . '/uploads/', $fileName
            );
        } else {
            $fileName = Auth::user()->photo;
        }
    	$data = $request->only(['photo', 'name', 'age', 'address', 'email', 'phone_number', 'service']);
    	$user = Auth::user()->update([
    		'name'			=> $data['name'],
    		'photo'			=> $fileName,
    		'age'			=> $data['age'],
    		'address'		=> $data['address'],
    		'email'			=> $data['email'],
            'phone_number'  => $data['phone_number'],
    		'service_id'	=> $data['service'],
    	]);
    	Session::flash('info', ['status' => true, 'message' => 'Your profile successfully updated.']);
    	return redirect('/');
    }

    public function rateEmployees(Request $request)
    {
    	$employee = User::find($request->input('employee_id'));
    	$employee->update([
    		'rating' => $employee->rating + $request->input('value')
    	]);
    	$employee->increment('rate_count');
    	return response()->json(['data' => 'oldu']);
    }

    public function confirm($id)
    {
        if ( ! Auth::check() )
            return redirect(url('/login'));

    	$employee = User::find($id);
    	$service = Service::find($employee->service_id);
    	return view('employees.confirm', compact('employee', 'service'));
    }

    public function cancel($id)
    {
    	$employee = User::find($id);
    	$service = Service::find($employee->service_id);
    	return view('employees.cancel', compact('employee', 'service'));
    }

    public function search(Request $request)
    {
        $q = $request->input('q');
        $users = User::where('name', 'LIKE', '%' . $q . '%')->get();
        return view('employees.index', compact('users'));
    }
}
