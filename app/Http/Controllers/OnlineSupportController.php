<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Message;
use App\Http\Requests;

class OnlineSupportController extends Controller
{
    public function sendMessage(Request $request)
    {
        $id = $request->input('send_to');
    	$userId = Auth::user()->id;
    	Message::create([
    		'send_to' => $id,
    		'user_id' => $userId,
    		'message' => $request->input('message')
    	]);
    	return response()->json(['message' => $request->input('message')]); 
    }

    public function getMessage()
    {
        if ( Auth::user()->is_admin )
        {
            return response()->json(['message' => Message::orderBy('id', 'DESC')->with('user')->first()]);    
        }
    	return response()->json(['message' => Message::where('user_id', Auth::user()->id)->orWhere('send_to', Auth::user()->id)->orderBy('id', 'DESC')->with('user')->first()]);
    }

    public function getAllMessages()
    {
        if ( Auth::user()->is_admin )
        {
            return response()->json(['messages' => Message::with('user')->get()]);
        }
    	return response()->json(['messages' => Message::where('user_id', Auth::user()->id)->orWhere('send_to', Auth::user()->id)->with('user')->get()]);
    }

    public function imageUpload(Request $request)
    {
        $fileName = md5($request->file('supportFile')) . '.' . $request->file('supportFile')->getClientOriginalExtension();
        $request->file('supportFile')->move(
            public_path() . '/uploads/', $fileName
        );

    	return response()->json(['a' => $fileName]);
    }

    public function videoUpload(Request $request)
    {
        $fileName = md5($request->file('supportFile')) . '.' . $request->file('supportFile')->getClientOriginalExtension();
        $request->file('supportFile')->move(
            public_path() . '/uploads/', $fileName
        );

        return response()->json(['a' => $fileName]);
    }

    public function audioUpload(Request $request)
    {
        $fileName = md5($request->file('supportFile')) . '.' . $request->file('supportFile')->getClientOriginalExtension();
        $request->file('supportFile')->move(
            public_path() . '/uploads/', $fileName
        );

        return response()->json(['a' => $fileName]);
    }
}
