<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\User;
use App\Http\Requests;

class ServicesController extends Controller
{
    public function index()
    {
    	$services = Service::get();
    	return view('services.index', compact('services'));
    }

    public function show($id)
    {
    	$users = User::where('service_id', $id)->get();
    	return view('employees.index', ['users' => $users, 'buttonName' => 'Pick']);
    }
}
