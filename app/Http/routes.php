<?php

function get_photo($name)
{
	return '/uploads/' . $name;
}

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::controllers([
    'auth' => '\App\Http\Controllers\Auth\AuthController',
    'password' => '\App\Http\Controllers\Auth\PasswordController',
]);

Route::get('/employees', 'EmployeesController@index');

Route::get('/edit_account', 'EmployeesController@edit');
Route::put('/update_account', 'EmployeesController@update');
Route::get('/rate_employees', 'EmployeesController@rateEmployees');
Route::get('/employees/{id}/confirm', 'EmployeesController@confirm');
Route::get('/employees/{id}/cancel', 'EmployeesController@cancel');

Route::post('/create_comment', 'CommentsController@store');
Route::get('/get_comments/{id}', 'CommentsController@show');

Route::get('/services', 'ServicesController@index');
Route::get('/services/{id}', 'ServicesController@show');

Route::post('/send_message', 'OnlineSupportController@sendMessage');
Route::post('/get_message', 'OnlineSupportController@getMessage');
Route::post('/get_all_messages', 'OnlineSupportController@getAllMessages');
Route::post('/image_upload', 'OnlineSupportController@imageUpload');
Route::post('/video_upload', 'OnlineSupportController@videoUpload');
Route::post('/audio_upload', 'OnlineSupportController@audioUpload');

Route::get('/admin', 'AdminController@home');
Route::get('/admin/send_invoice', 'AdminController@sendInvoice');
Route::post('/admin/send_invoice', 'AdminController@sendInvoicePost');
Route::get('/admin/services', 'AdminController@getServices');
Route::post('/admin/services', 'AdminController@postServices');

Route::get('/search', 'EmployeesController@search');

Route::get('/contact', function() {
	return view('contact');
});

Route::post('/contact', function(){
	// Mail::send('emails.welcome', [], function($message)
	// {
	//     $message->to(Request::input('email'), Request::input('email'))->subject('Contact!');
	// });
	Session::flash('info', ['status' => true, 'message' => 'Thanks for your feedback.']);
	return redirect('/contact');
});
