<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address');
            $table->string('phone_number');
            $table->integer('age');
            $table->tinyInteger('is_admin');
            $table->integer('rate_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->drop('address');
            $table->drop('phone_number');
            $table->drop('age');
            $table->drop('is_admin');
            $table->drop('rate_count');
        });
    }
}
