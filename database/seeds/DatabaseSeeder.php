<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	DB::table('users')->insert([
		'name' 		=> 'W-fix Admin',
		'email'		=> 'admin@wfix.com',
		'password' 	=> bcrypt('w-fix++'),
		'address' 	=> 'Gayrettepe',
		'phone_number' 	=> '555 555 55 55',
		'age'		=> '25',
		'is_admin'	=> 1
	]);
        // $this->call(UsersTableSeeder::class);
    }
}
